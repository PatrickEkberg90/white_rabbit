<?php

class WhiteRabbit
{
    public function findMedianLetterInFile($filePath)
    {
        return array("letter"=>$this->findMedianLetter($this->parseFile($filePath),$occurrences),"count"=>$occurrences);
    }

    /**
     * Parse the input file for letters.
     * @param $filePath
     */
    private function parseFile ($filePath)
    {
	
        //TODO implement this!
		$letters = array_fill_keys(range('a', 'z'), 0);

		$text = fopen($filePath, "r");
		while(!feof($text)){ 
			$letter = strToLower(fgetc($text));
			if(array_key_exists($letter, $letters)){
				$letters[$letter]++;
			}
		}
		return $letters;
    }

    /**
     * Return the letter whose occurrences are the median.
     * @param $parsedFile
     * @param $occurrences
     */
    private function findMedianLetter($parsedFile, &$occurrences)
    {
        //TODO implement this!

		asort($parsedFile);
		$letters = array_keys($parsedFile);
		$occurrences = $parsedFile[$letters[count($letters)/2 - 1]];

		return $letters[count($letters)/2 - 1];
    }
}

?>